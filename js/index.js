function clone(obj){ 
    let cloneObj = {}; //создаем пустой объект cloneObj
    
    if(typeof obj !== 'object' || obj == null){ // делаем проверку, что принимаемый тим данных объект, 
        return obj; //если нет то возвращаем исходные данные;
    }
    
    for(let key in obj){//делаем перебор ключей
        if(obj.hasOwnProperty(key)){//проверяем на наличие coбственных, а не наследуемых свойств;
            cloneObj[key] = clone(obj[key]);//присваиваем свойства из клонируемого объекта в клон;
        }
    }
    
    return cloneObj;  // возвращаем склонированый объект  
}


let originalObj = {
	name : "Misha",
	age: 16,
	smth:{
		city:"Kiyv"
		}
};

let clonedObj = clone(originalObj);//при создании cloneObj вызываем функцию clone в которую передаум объект из которого нужно взять ключи и скопировать в cloneObj;

clonedObj.smth.city = "another"

console.log(originalObj);
console.log(clonedObj);

// let [ , , title] = "Юлий Цезарь Император Рима".split(' ');

// console.log(title); 

// let user = {
//     name: "John",
//     sex: 'male',
//     age: 20,
//     isAdmin: true
// }

// let {name: n, sex: s, age: a, isAdmin} = user;

//  console.log(n);
//  console.log(s);
//  console.log(a);
//  console.log(isAdmin);

// 

// let salaries = { // проверка, кто больше всего зарабатывает в компании;
//     // "Вася": 100,
//     // "Петя": 300,
//     // "Даша": 250
// };

// let max = 0, maxName = '';

// for (name in salaries) {
//     if (max < salaries[name]){
//         max = salaries[name];
//         maxName = name;
//     }
// }

// console.log (maxName || "Нет сотрудников");


// let menu = {
//     width: 200,
//     height: 300,
//     title: "My menu"
//   };

// function isNumeric(n) {
//     return !isNaN(parseFloat(n)) && isFinite(n); // функция проверки на число
//   }
// function multiplayNumeric(obj) {
//     for(key in obj) {
//         if (isNumeric(obj[key])) {
//             obj[key] *= 2;
//         }
//     }
//     return console.log(obj);
// }

// multiplayNumeric(menu);